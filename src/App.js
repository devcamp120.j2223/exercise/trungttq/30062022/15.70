import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from  "reactstrap"
import { LoginForm } from './components/LoginForm';

function App() {
  return (
    <Container>
      <LoginForm/>
    </Container>
  );
}

export default App;
