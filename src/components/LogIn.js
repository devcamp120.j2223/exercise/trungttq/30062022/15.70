import { Row, Col, Button } from "reactstrap";
import { useState } from "react";

function LogIn() {
  // LOG IN
  const [emailInput, setEmailInput] = useState("Email Address");
  const [passwordInput, setPassword] = useState("Password");

  const changeInputEmailHandler = (event) => {
    setEmailInput(event.target.value);
  };

  const changeInputPasswordHandler = (event) => {
    setPassword(event.target.value);
  };

  const buttonLogInHandler = () => {
    console.log("Nút Log In được bấm");
    console.log("Email: ", emailInput);
    console.log("Password:", passwordInput);
  };

  return (
    <div>
      <h2 className="mt-5"> Welcome Back!</h2>

      <Row>
        <Col className="mt-5">
          <input
            type="text"
            className="w-100"
            onChange={changeInputEmailHandler}
            placeholder={emailInput}
          />
        </Col>
      </Row>

      <Row>
        <Col className="mt-4">
          <input
            type="password"
            className="w-100"
            onChange={changeInputPasswordHandler}
            placeholder={passwordInput}
          />
        </Col>
      </Row>

      <Row>
        <Col className="mt-3">
          <p
            style={{ textAlign: "right", fontFamily: "TiTi", color: "#1ab188" }}
          >
            Forgot Password?
          </p>
        </Col>
      </Row>

      <Row className="d-flex align-items-center justify-content-center mt-2">
        <Col>
          <Button
            className="w-100 p-3"
            style={{ backgroundColor: "#1ab188" }}
            onClick={buttonLogInHandler}
          >
            LOG IN
          </Button>

        </Col>
      </Row>
    </div>
  );
}

export default LogIn;
