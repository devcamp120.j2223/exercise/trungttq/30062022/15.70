import { Row,Col,Nav, NavItem } from "reactstrap"
import './loginform.css'
import LogIn from "./LogIn";
import SignUp from "./SignUp";
import { useState } from "react";

export function LoginForm() {
   // MENU
   const [showForm, setShowForm] = useState(true);

   const LogInMenu = () => {
       setShowForm(true);
   }

   const SignUpMenu = () => {
       setShowForm(false);
   }

    return(
        <div>
        <Row className="d-flex align-items-center">
            <Col sm={4} md={4} lg={4} xs={4} className="text-center mx-auto bg-content p-5">
                <Row className="d-flex align-items-center justify-content-center mt-4">
                    <Nav  className="d-flex align-items-center justify-content-center">
                        <NavItem 
                        className="w-50 p-2"  
                        style={{backgroundColor: showForm ? "#435359" : "#1ab188", color: "white", cursor: 'pointer'}}  
                        onClick={SignUpMenu}
                        >
                            Sign Up
                        </NavItem>
                        <NavItem 
                        className="w-50 p-2" 
                        style= {{backgroundColor: showForm ? "#1ab188" : "#435359", color: "white", cursor: 'pointer'}}
                        onClick={LogInMenu}
                        >
                            Log In
                        </NavItem>

                    </Nav>
                </Row>

               { showForm  ?  <LogIn /> : <SignUp  />   }

            </Col>
        </Row>
        </div>
       

    )
}