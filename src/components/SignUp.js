import { Row, Col, Button } from "reactstrap";
import { useState } from "react";

function SignUp() {
  // SIGN UP
  const [firstName, setFirstName] = useState("First Name");
  const [lastName, setLastName] = useState("Last Name");
  const [emailSignUp, setEmailSingUp] = useState("Email Address");
  const [passwordSignUp, setPasswordSignUp] = useState("Set A Password");

  const changeFirstNameHandler = (event) => {
    setFirstName(event.target.value);
  };

  const changeLastNameHandler = (event) => {
    setLastName(event.target.value);
  };

  const changeSignUpEmailHandler = (event) => {
    setEmailSingUp(event.target.value);
  };

  const changeSignUpPasswordHandler = (event) => {
    setPasswordSignUp(event.target.value);
  };

  const buttonSignUp = () => {
    console.log("Nút Sign Up được bấm");
    console.log("First name: ", firstName);
    console.log("Last name: ", lastName);
    console.log("Email:", emailSignUp);
    console.log("Password:", passwordSignUp);
  };

  return (
    <div>
      <h2 className="mt-5"> Sign Up for Free </h2>

      <Row>
        <Col sm={6} className="mt-5">
          <input
            type="text"
            className="w-100"
            onChange={changeFirstNameHandler}
            placeholder={firstName}
          ></input>
        </Col>

        <Col sm={6} className="mt-5">
          <input
            type="text"
            className="w-100"
            onChange={changeLastNameHandler}
            placeholder={lastName}
          ></input>
        </Col>
      </Row>

      <Row>
        <Col className="mt-4">
          <input
            type="text"
            className="w-100"
            onChange={changeSignUpEmailHandler}
            placeholder={emailSignUp}
          ></input>
        </Col>
      </Row>

      <Row>
        <Col className="mt-4">
          <input
            type="password"
            className="w-100"
            onChange={changeSignUpPasswordHandler}
            placeholder={passwordSignUp}
          ></input>
        </Col>
      </Row>

      <Row className="d-flex align-items-center justify-content-center mt-5">
        <Col sm={12}>
          <Button
            className="w-100 p-3"
            style={{ backgroundColor: "#1ab188" }}
            onClick={buttonSignUp}
          >
            GET STARTED
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default SignUp;
